﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Shared
{
    [ServiceContract(Namespace="urn:ps")]
   public interface IMathNigel
    {

        [OperationContract]
        int SubtactNumbers(int a, int b);
        [OperationContract]
        int AddNumbers(int a, int b);


    }
    public interface IIMathNigelChannel : IMathNigel, IClientChannel { }
}
