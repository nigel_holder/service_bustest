﻿using Microsoft.ServiceBus;
using Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace ServiceBusReciever
{
    class Program
    {
        static void Main(string[] args)
        {
            var cf = new ChannelFactory<IIMathNigelChannel>(
       new NetTcpRelayBinding(),
       new EndpointAddress(ServiceBusEnvironment.CreateServiceUri("sb", "nholder", "solver")));

            cf.Endpoint.Behaviors.Add(new TransportClientEndpointBehavior
            { TokenProvider = TokenProvider.CreateSharedAccessSignatureTokenProvider("RootManageSharedAccessKey", "dySHP/jB5hFU8QYpBjzpG3cB2h+ciB+pnC8QCMDSdrs=") });

            using (var ch = cf.CreateChannel())
            {
                Console.WriteLine(ch.AddNumbers(4, 5));
                Console.WriteLine(ch.SubtactNumbers(150, 5));
                Console.WriteLine("Press ENTER to close");
                Console.ReadLine();
            }
        }
    }
}
