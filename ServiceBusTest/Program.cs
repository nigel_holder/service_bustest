﻿using Microsoft.ServiceBus;
using Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace ServiceBusTest
{
    class Program
    {
        static void Main(string[] args)
        {
            ServiceHost sh = new ServiceHost(typeof(MathNigel));

            sh.AddServiceEndpoint(
               typeof(IMathNigel), new NetTcpBinding(),
               "net.tcp://localhost:9358/solver");

            sh.AddServiceEndpoint(
               typeof(IMathNigel), new NetTcpRelayBinding(),
               ServiceBusEnvironment.CreateServiceUri("sb", "nholder", "solver"))
                .Behaviors.Add(new TransportClientEndpointBehavior
                {
                    TokenProvider = TokenProvider.CreateSharedAccessSignatureTokenProvider("RootManageSharedAccessKey", "dySHP/jB5hFU8QYpBjzpG3cB2h+ciB+pnC8QCMDSdrs=")
                });

            sh.Open();

            Console.WriteLine("Press ENTER to close");
            Console.ReadLine();

            sh.Close();
        }
    }
}
